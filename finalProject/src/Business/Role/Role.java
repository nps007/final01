/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.HealthCareInternetSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;

/**
 *
 * @author nikhil
 */
public abstract class Role {
    
    public enum RoleType{
        SystemAdmin("System Admin"),
        Admin("Admin"),
        Doctor("Doctor"),
        LabAssistant("Lab Assistant"),
        AdministrativeStaff("Administrative Staff"),
        Nurse("Nurse"),
        Pharmacy("Pharmacy"),
        Patient("Patient"),
        DrugCompany("DrugCompany"),
        FDA("FDA");
        
        private String value;
        private RoleType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
    
    public abstract JPanel createWorkArea(JPanel userProcessContainer, 
            UserAccount account, 
            Organization organization, 
            Enterprise enterprise, 
            HealthCareInternetSystem business);
    public abstract RoleType getRoleType();

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
    
}