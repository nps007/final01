/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccount;

import Business.Employee.Employee;
import Business.Patient.Patient;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author nikhil
 */
public class UserAccountDirectory {
    
    private ArrayList<UserAccount> userAccountList;

    public UserAccountDirectory() {
        userAccountList = new ArrayList<>();
    }

    public ArrayList<UserAccount> getUserAccountList() {
        return userAccountList;
    }
    
    public UserAccount authenticateUser(String username, String password){
        for (UserAccount ua : userAccountList)
            if (ua.getUsername().equals(username) && ua.getPassword().equals(password)){
                return ua;
            }
        return null;
    }
    
    public PatientAccount createPatientUserAccount(String username, String password, Patient patient, Role role){
        PatientAccount patientAccount = new PatientAccount();
        patientAccount.setUsername(username);
        patientAccount.setPassword(password);
        patientAccount.setPatient(patient);
        patientAccount.setRole(role);
        userAccountList.add(patientAccount);
        return patientAccount;
    } 
    
    public EmployeeAccount createEmployeeUserAccount(String username, String password, Employee employee, Role role){
        EmployeeAccount userAccount = new EmployeeAccount();
        userAccount.setUsername(username);
        userAccount.setPassword(password);
        userAccount.setEmployee(employee);
        userAccount.setRole(role);
        userAccountList.add(userAccount);
        return userAccount;
    }
    
   public void deleteUserAccount(UserAccount ua){
       userAccountList.remove(ua);
   }
}
