/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Business.Organization;

import Business.Patient.PatientDirectory;
import Business.Role.PatientRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author nikhil
 */
public class PatientOrganization extends Organization{

    private PatientDirectory patientDir;
    public PatientOrganization() {
        super(Organization.Type.Patient.getValue());
        this.patientDir = new PatientDirectory();
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new PatientRole());
        return roles;
    }

    @Override
    public Type getOrganizationType() {
        return Organization.Type.Patient;
    }

    public PatientDirectory getPatientDir() {
        return patientDir;
    }
    
}
