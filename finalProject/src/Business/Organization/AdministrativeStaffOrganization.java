/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Business.Organization;

import Business.Role.AdministrativeStaffRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author nikhil
 */
public class AdministrativeStaffOrganization extends Organization{

    public AdministrativeStaffOrganization() {
        super(Organization.Type.AdministrativeStaff.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new AdministrativeStaffRole());
        return roles;
    }

    @Override
    public Type getOrganizationType() {
        return Organization.Type.AdministrativeStaff;
    }
    
}
