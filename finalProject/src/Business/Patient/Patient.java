/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Business.Patient;

import java.util.ArrayList;

/**
 *
 * @author nikhil
 */
public class Patient {
    private String patientId;
    private String name;
    private String sex;
    private String prefferedLanguage;
    private String smokingStatus;
    private String problems;
    private String email;
    private String phoneNumber;
    private int age;
    private String dataOfBirth;
    private AllergyInformation allergyInformation;
    private VitalSignHistory vitalSignHistory;
    private DignosisHistory dignosisHistory;
    private ArrayList<String> labTestAndReports = new ArrayList();
    private ArrayList<String> patientMedications = new ArrayList();

    public Patient() {
        allergyInformation = new AllergyInformation();
        vitalSignHistory = new VitalSignHistory();
        dignosisHistory = new DignosisHistory();
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    public String getProblems() {
        return problems;
    }

    public void setProblems(String problems) {
        this.problems = problems;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public VitalSignHistory getVitalSignHistory() {
        return vitalSignHistory;
    }

    public void setVitalSignHistory(VitalSignHistory vitalSignHistory) {
        this.vitalSignHistory = vitalSignHistory;
    }
    
    public ArrayList<String> getLabTestHistory() {
        return labTestAndReports;
    }

    public void setLabTestHistory(ArrayList labTestAndReports) {
        this.labTestAndReports = labTestAndReports;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDataOfBirth() {
        return dataOfBirth;
    }

    public void setDataOfBirth(String dataOfBirth) {
        this.dataOfBirth = dataOfBirth;
    }

    public String getPrefferedLanguage() {
        return prefferedLanguage;
    }

    public void setPrefferedLanguage(String prefferedLanguage) {
        this.prefferedLanguage = prefferedLanguage;
    }

    public String getSmokingStatus() {
        return smokingStatus;
    }

    public void setSmokingStatus(String smokingStatus) {
        this.smokingStatus = smokingStatus;
    }

    public AllergyInformation getAllergyInformation() {
        return allergyInformation;
    }

    public void setAllergyInformation(AllergyInformation allergyInformation) {
        this.allergyInformation = allergyInformation;
    }

    public ArrayList<String> getPatientMedications() {
        return patientMedications;
    }

    public void setPatientMedications(ArrayList<String> patientMedications) {
        this.patientMedications = patientMedications;
    }

    @Override
    public String toString() {
        return name;
    }

    public DignosisHistory getDignosisHistory() {
        return dignosisHistory;
    }
    
    
}
