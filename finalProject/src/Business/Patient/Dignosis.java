/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Business.Patient;

/**
 *
 * @author nikhil
 */
public class Dignosis {
    private String problem;
    private String dignosisInfo;
    private String treatmentDate;
    private String doctorName;

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getDignosisInfo() {
        return dignosisInfo;
    }

    public void setDignosisInfo(String dignosisInfo) {
        this.dignosisInfo = dignosisInfo;
    }

    public String getTreatmentDate() {
        return treatmentDate;
    }

    public void setTreatmentDate(String treatmentDate) {
        this.treatmentDate = treatmentDate;
    }

    @Override
    public String toString() {
        return problem;
    }
    
    
}
