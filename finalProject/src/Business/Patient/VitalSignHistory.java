/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Patient;

import java.util.ArrayList;

/**
 *
 * @author nikhil
 */
public class VitalSignHistory {

    ArrayList<VitalSign> vitalSignList = new ArrayList();

    public VitalSignHistory() {
        vitalSignList = new ArrayList();
    }

    public ArrayList<VitalSign> getVitalSignList() {
        return vitalSignList;
    }

    public void setVitalSignList(ArrayList<VitalSign> vitalSignList) {
        this.vitalSignList = vitalSignList;
    }

    public VitalSign AddVitalSign() {
        VitalSign vitalSign = new VitalSign();
        vitalSignList.add(vitalSign);
        return vitalSign;
    }

    public void removeVitalSign(VitalSign vitalSign) {
        vitalSignList.remove(vitalSign);
    }
}
