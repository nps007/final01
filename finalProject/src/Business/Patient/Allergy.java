/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Business.Patient;

/**
 *
 * @author nikhil
 */
public class Allergy {
    private AllergyType type;
    private String description;

    public Allergy(AllergyType type, String description) {
        this.type = type;
        this.description = description;
    }
    
     public enum AllergyType{
        DrugAllergy("Drug"),
        FoodAllergy("Food");
        
        private String value;

        private AllergyType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public AllergyType getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return type.value;
    }
    
    
}
