/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Business.Patient;

import java.util.ArrayList;

/**
 *
 * @author nikhil
 */
public class PatientDirectory {
    ArrayList<Patient> patientList;

    public PatientDirectory() {
        patientList = new ArrayList();
    }

    public ArrayList<Patient> getPatientList() {
        return patientList;
    }
    
    public Patient searchPatient(String patientName){
        for(Patient p: patientList){
            if(p.getName().equalsIgnoreCase(patientName)){
                return p;
            }
        }
        return null;
    }
    
}
