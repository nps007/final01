/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Business.Drug;

import Business.Role.Role;

/**
 *
 * @author nikhil
 */
public class DrugFeedback {
    private String adverseEvent;
    private String detailEventDescription;
    private String feedbackProvider;
    private String personRole;
    private String state;
    private String severity;

    public String getDetailEventDescription() {
        return detailEventDescription;
    }

    public void setDetailEventDescription(String detailEventDescription) {
        this.detailEventDescription = detailEventDescription;
    }
    
    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }
    
    public String getAdverseEvent() {
        return adverseEvent;
    }

    public void setAdverseEvent(String adverseEvent) {
        this.adverseEvent = adverseEvent;
    }

    public String getFeedbackProvider() {
        return feedbackProvider;
    }

    public void setFeedbackProvider(String feedbackProvider) {
        this.feedbackProvider = feedbackProvider;
    }

    public String getPersonRole() {
        return personRole;
    }

    public void setPersonRole(String personRole) {
        this.personRole = personRole;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return adverseEvent;
    }
    
    
}
