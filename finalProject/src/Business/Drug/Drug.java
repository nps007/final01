/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Business.Drug;

import java.util.ArrayList;

/**
 *
 * @author nikhil
 */
public class Drug {
    private String drugName;
    private String safetyIssues;
    private ArrayList<String> manufacturerList;
    private boolean fdaApproved = false;
    private ArrayList<DrugFeedback> drugFeedbacks;
    private int fatalAdverseEventCount = 0;

    public int getFatalAdverseEventCount() {
        return fatalAdverseEventCount;
    }

    public void incrementFatalAdverseEventCount(){
        fatalAdverseEventCount ++;
    }
    
    public Drug() {
        manufacturerList = new ArrayList();
        drugFeedbacks = new ArrayList();
    }
    
    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getSafetyIssues() {
        return safetyIssues;
    }

    public void setSafetyIssues(String safetyIssues) {
        this.safetyIssues = safetyIssues;
    }

    public ArrayList<String> getManufacturerList() {
        return manufacturerList;
    }

    public void setManufacturerList(ArrayList<String> manufacturerList) {
        this.manufacturerList = manufacturerList;
    }

    public boolean isFdaApproved() {
        return fdaApproved;
    }

    public void setFdaApproved(boolean fdaApproved) {
        this.fdaApproved = fdaApproved;
    }

    public ArrayList<DrugFeedback> getDrugFeedbacks() {
        return drugFeedbacks;
    }

    public void setDrugFeedbacks(ArrayList<DrugFeedback> drugFeedbacks) {
        this.drugFeedbacks = drugFeedbacks;
    }

    @Override
    public String toString() {
        return drugName;
    }
}
