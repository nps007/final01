package Business;

import Business.Employee.Employee;
import Business.Role.SystemAdminRole;
import Business.UserAccount.UserAccount;

/**
 *
 * @author nikhil
 */
public class ConfigureASystem {
    
    public static HealthCareInternetSystem configure(){
        
        HealthCareInternetSystem system = HealthCareInternetSystem.getInstance();
        
        //Create a network
        //create an enterprise
        //initialize some organizations
        //have some employees 
        //create user account
        
        
        Employee employee = system.getEmployeeDirectory().createEmployee("Nikhil Shankar");
        
        UserAccount ua = system.getUserAccountDirectory().createEmployeeUserAccount("sysadmin", "sysadmin", employee, new SystemAdminRole());
        
        return system;
    }
    
}
