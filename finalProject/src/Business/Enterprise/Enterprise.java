/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Organization.Organization;
import Business.Organization.OrganizationDirectory;
import Business.Role.AdminRole;
import Business.Role.DrugCompanyRole;
import Business.Role.FDARole;
import Business.Role.Role;
import Business.UserAccount.UserAccount;

/**
 *
 * @author nikhil
 */
public abstract class Enterprise extends Organization{
    private String networkName;
    private String state;
    private EnterpriseType enterpriseType;
    private OrganizationDirectory organizationDirectory;
    
    public Enterprise(String name, EnterpriseType type, String state) {
        super(name);
        this.enterpriseType = type;
        this.state = state;
        organizationDirectory = new OrganizationDirectory();
    }
    
    public enum EnterpriseType{
        Hospital("Hospital"),
        DrugCompany("DrugCompany"),
        FDA("FDA");
        
        private String value;

        private EnterpriseType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public OrganizationDirectory getOrganizationDirectory() {
        return organizationDirectory;
    }

    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName;
    }
    
    public boolean isEnterpriseAdminExists(){
        for(UserAccount ua: this.getUserAccountDirectory().getUserAccountList()){
            if(ua.getRole() instanceof AdminRole || ua.getRole() instanceof FDARole 
                    || ua.getRole() instanceof DrugCompanyRole){
                return true;
            }
        }
        return false;
    }
    
    public UserAccount getEnterpriseAdmin(){
        for(UserAccount ua: this.getUserAccountDirectory().getUserAccountList()){
            if(ua.getRole().getRoleType().equals(Role.RoleType.Admin)){
                return ua;
            }
        }
        return null;
    }
   }
