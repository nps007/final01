/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Business.Enterprise;

import Business.Organization.Organization;
import Business.Role.DrugCompanyRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author nikhil
 */
public class DrugCompanyEnterprise extends Enterprise{

    private ArrayList<String> manufacturingDrugList;
    
    public DrugCompanyEnterprise(String name, String state) {
        super(name, EnterpriseType.DrugCompany, state);
        manufacturingDrugList = new ArrayList();
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new DrugCompanyRole());
        return roles;
    }

    public ArrayList<String> getManufacturingDrugList() {
        return manufacturingDrugList;
    }
    

    @Override
    public Type getOrganizationType() {
        return Organization.Type.DrugCompany;
    }
    
    
}
